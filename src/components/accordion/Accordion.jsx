import { useState, useEffect } from "react";
import appStyle from "./accordion.module.css";

function Accordion({ textForAccord }) {
  const [panels, setPanels] = useState([]);

  useEffect(() => {
    textForAccord && setPanels(textForAccord);
  }, [textForAccord]);

  const handleClick = (id) => {
    setPanels((prevPanels) =>
      prevPanels.map((panel) =>
        panel.panelId === id ? { ...panel, clicked: !panel.clicked } : panel
      )
    );
  };

  return (
    <>
      <div className={appStyle.harmonica}>
        {panels && panels.map((panel) => (
          <div className={appStyle.panelContainer} key={panel.panelId}>
            <button
              className={appStyle.headerTrigger}
              onClick={() => handleClick(panel.panelId)}
            >
              {panel.header}
            </button>
            <div
              className={`${appStyle.item} ${
                panel.clicked ? appStyle.open : ""
              }`}
            >
              <p>{panel.content}</p>
            </div>
          </div>
        ))}
      </div>
    </>
  );
}

export default Accordion;
