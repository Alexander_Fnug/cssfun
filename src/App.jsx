import { useEffect, useState } from "react";

import Accordion from "./components/accordion/Accordion";

function App() {
const [textForAccord, setTextForAccord] = useState([])

useEffect(() => {
  const fetchJson = async () => {
    try {
      const response = await fetch('/textForAccord.json');
      const data = await response.json();
      setTextForAccord(data);
    } catch (error) {
      console.log('Error fetching JSON:', error);
    }
  };
  fetchJson(); 
}, []);

console.log(textForAccord)
  return (
    <>
     <Accordion textForAccord={textForAccord.panels}/>
    </>
  );
}

export default App;
